//Program to demonstrate creation of threads using thread class
public class ThreadClass
{
 public static void main(String[] args)
 {
   MyThread1 obj = new MyThread1();
   obj.start(); 
 }

}
class MyThread1 extends Thread
{
    public void run()
    {
        for(int i=0; i<5; i++)
        System.out.println(i);
    }
}