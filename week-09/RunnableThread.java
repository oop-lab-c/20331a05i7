//Program to demonstrate creation of threads using runnable interface
public class RunnableThread {
    public static void main(String[] args)
    {
      MyThread obj = new MyThread();
      Thread ob = new Thread(obj);
      ob.start();     //start() : built-in method in thread
    }
}
class MyThread implements Runnable //MyThread class implements runnable interface
{
    public void run()
    {
        for(int i=0; i<7; i++)
        System.out.println(i*i);
    }
}
