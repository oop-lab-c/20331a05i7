//program to demonstrate partial abstraction in java//
abstract class Base {
    public
    Base(){
        System.out.println("Base Constructor called");
    }
	abstract void fun();
    void display(){
        System.out.println("This is not an abstract function");
    }

}
class Derived extends Base {
	void fun()
	{
		System.out.println("Derived function() called");
	}
}
class  ParAbs{
public static void main(String args[])
	{

		
		Derived b = new Derived();
		b.fun();
        b.display();

	}
}









