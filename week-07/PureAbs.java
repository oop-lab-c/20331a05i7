//program to demonstrate pure abstraction in java//
import java.util.*;
interface Animal{
public void bark();
public void walk();

}
class Dog implements Animal{
public void bark(){
System.out.println("the dog barks bow");
}
public void walk(){
System.out.println("the dog walks");
}
}
public class PureAbs{
public static void main(String[] args){
Dog d=new Dog();
d.bark();
d.walk();
}
} 
