//program to know the use of virtual function performing pure abstraction//
#include<iostream>
using namespace std;
class Animal{
public:
virtual void bark()=0;
virtual void walk()=0;
};
class Dog : public Animal{
public:
void bark(){
cout<<"It barks"<<endl;
}
void walk(){
cout<<"It moves forward"<<endl;
}
};
int main(){
Dog obj;
obj.bark();
obj.walk();
}