class A{
    void method() {
    System.out.println("Method A");
    }
}
class B extends A {
    void method() {
    System.out.println("Method B");
    }
}
class C extends B {
    void method(){
    System.out.println("Method C");
    }
}
public class MethodOLINheri{
    
    public static void main(String[] args) {
    A objA = new A();
    B objB = new B();
    C objC = new C();
    
    objA.method(); // invoke the method() of class A
    objB.method(); // invoke the method() of class B
    objC.method(); // invoke the method() of class C
    }
}