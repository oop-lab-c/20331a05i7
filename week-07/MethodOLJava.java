public class MethodOLJava{
  void display(int a){
    System.out.println("Arguments: " + a);
    }
  void display(int a, int b){
    System.out.println("Arguments: " + a + " and " + b);
    }
public static void main(String[] args) {
    MethodOLJava m = new MethodOLJava();
    m.display(1);
    m.display(1, 4);
}
}
