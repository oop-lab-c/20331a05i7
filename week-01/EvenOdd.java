//program to check the number whether it is even or odd//
import java.util.*;
public class EvenOdd{
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a Number : ");
        int num = in.nextInt();
        if(num == 0){                                     //if-else ladder to check whether the number is even or odd//
            System.out.println("Number is 0 which is neither Even nor Odd");
        }else if(num % 2 != 0){
            System.out.println("num is an ODD Number");
        }else{
            System.out.println("num is an EVEN Number");
        }
    }
    
}