//program to demostrate different types of Inheritance in java//
class Vehicle {
  public
    Vehicle()
    {
      System.out.println("This is Vehicle");
    }
}
class FourWheeler{
    public
    FourWheeler(){
        System.out.println ("This is fourwheeler Vehicle");
    }

}
class simpleIn extends Vehicle{
    public
    simpleIn(){
        System.out.println("Base class is Vehicle");
    }
    

}

class twoWheeler extends Vehicle{
    public
    twoWheeler(){
        System.out.println("Base Class is Vehicle");
        System.out.println("this is  two wheeler Vehicle");
    }

}
class bike extends twoWheeler{
    public
    bike(){
        System.out.println("Base class is twoWheeler");
        System.out.println("bike is two wheeler and Vehicle");
    }

}
class bicycle extends twoWheeler{
    public
    bicycle(){
        System.out.println("Base class is twoWheeler");
        System.out.println("bicycle is  two Wheeler and Vehicle");
    }
}
public class InheriTypes{
public static void main(String[] args){
    System.out.println("Simple Inheritence");
    simpleIn obj1 = new simpleIn();
    
    System.out.println("multilevel Inheritence");
    bike obj2 =new bike();
    System.out.println("heirarcial Inheritence");
    bike obj3=new bike();
    bicycle obj4 =new bicycle();
}
}
























