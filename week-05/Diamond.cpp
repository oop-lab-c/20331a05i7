//program to demonstrate the diamond problem//
#include<iostream>
using namespace std;
class Vehicle {

public:
	Vehicle() 
    { 
        cout << "Constructor  Vehicle called" << endl; 
    }
};

class Car : public Vehicle {

public:
	Car() {
	cout<<"Constructor Car called"<< endl;
	}
};

class Van : public Vehicle {

public:
	Van() {
		cout<<"Constructor  Van called"<< endl;
	}
};

class Fuel : public Car, public Van {
public:
	Fuel(){
		cout<<"Constructor  Fuel called"<< endl;
	}
};

int main() {
    Fuel obj;
}
