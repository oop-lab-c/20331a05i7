//program to demonstrate simple inheritance//
import java.util.*;
    class Student{
        int regdno;
        void getNo(int no){
            regdno=no;
        }
        void putNo(){
            System.out.println("reg njumber="+regdno);
        }
    }
    class Marks extends Student{
        float marks;
        void getMarks(float m){
            marks=m;
        }
        void putmarks(){
            System.out.println("marks="+marks);
        }
    }
    public class simpinher{
    public static void main(String[] args){
        Marks obj = new Marks();
        obj.getNo(25);
        obj.putNo();
        obj.getMarks(85);
        obj.putmarks();
    }
}
    