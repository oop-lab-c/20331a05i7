#include<iostream>
using namespace std;
class Base{
    private:
    int pvt=5;
    protected:
    int prt=10;
    public:
    int pub=15;
    int getPVT(){
        return pvt;
    }
};
class derived:public Base {
     public:
     void display(){
     cout<<"private Variable inaccessible in derived class."<<endl;
     cout<<"Proteced Variable:"<<prt<<endl;
     cout<<"public Variable:"<<pub<<endl;
     }
     int getPRT(){
         return prt;
     }

 };
class derived2: protected Base{
    public:
        void display(){
        
        cout<<"private variable is inaccessible in derived class."<<endl;
        cout<<"protected Variable:"<<prt<<endl;
        cout<<"public Variable:"<<pub<<endl;
    }
    int getPRT(){
        return prt;
    }
    int getPUB(){
        return pub;
    }
    int PRI(){
        return getPVT();
    }

};
class derived3: private Base{
    public:
    void display(){
        cout<<"private Variable is inaccessible in derived class"<<endl;
        cout<<"public :"<<pub<<endl;
        cout<<"protected:"<<prt<<endl;
    }
    int getPRT(){
        return prt;
    }
    int getPUB(){
        return pub;
    }
    int PRI2(){
        return getPVT();
    }


};
 int main(){
     cout<<"public Iheritence"<<endl;
     derived obj;
     obj.display();
     cout<<"public "<<obj.pub<<endl;
     cout<<"protect"<<obj.getPRT()<<endl;
     cout<<"private "<<obj.getPVT()<<endl;
     cout<<"protected Inheritence"<<endl;
     derived2 obj2;
     obj2.display();
     cout<<"protected"<<obj2.getPRT()<<endl;
     cout<<"public"<<obj2.getPUB()<<endl;
     cout<<"private"<<obj2.PRI()<<endl;
     cout<<"Private Inheritence"<<endl;
     derived3 obj3;
     obj3.display();
     cout<<"public "<<obj3.getPUB()<<endl;
     cout<<"protected "<<obj3.getPRT()<<endl;
     cout<<"private "<<obj3.PRI2()<<endl;

    return 0;
 }