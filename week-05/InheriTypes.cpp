//program to demonstrate different types of inheritance//
#include<iostream>
using namespace std;
class Vehicle {
  public:
    Vehicle()
    {
      cout << "This is  Vehicle\n";
    }
};
class FourWheeler{
    public:
    FourWheeler(){
        cout << "This is a fourwheeler Vehicle\n";
    }

};
class simpleIn:public Vehicle{       //simple Inheritance//
    public:
    simpleIn(){
        cout<<"Base class is Vehicle"<<endl;
    }
    

};
class car:public Vehicle,public FourWheeler{              
    public:
    car(){
        cout<<"Base class is Vehicle,FourWheeler"<<endl;         
        cout<<"This is car\n";
    }
   
};
class twoWheeler:public Vehicle{                      
    public:
    twoWheeler(){
        cout<<"Base Class is Vehicle\n";
        cout<<"this is  two wheeler Vehicle\n";
    }

};
class bike:public twoWheeler{
    public:
    bike(){
        cout<<"Base class is twoWheeler\n";
        cout<<"bike is two wheeler and Vehicle\n";
    }

};
class bicycle:public twoWheeler{
    public:
    bicycle(){
        cout<<"Base class is twoWheeler\n";
        cout<<"bicycle is  two Wheeler and Vehicle\n";
    }
};
int main(){
    cout<<"Simple Inheritence"<<endl;
    simpleIn obj1;
    cout<<"multiple Inheritence\n";
    car obj2;
    cout<<"multilevel Inheritence\n";
    bike obj3;
    cout<<"heirarcial Inheritence\n";
    bike obj4;
    bicycle obj5;


}