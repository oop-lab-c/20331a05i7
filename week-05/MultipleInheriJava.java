// Java program to demonstrate How Diamond Problem

// Interface 1
interface GPI {
	default void show()
	{
		System.out.println("Default GPI");
	}
}

// Interface 2
interface PI1 extends GPI {
}
interface PI2 extends GPI {
}
class MultipleInheriJava implements PI1, PI2 {

	// Main driver method
	public static void main(String args[])
	{

		// Creating object of this class
		// in main() method
        MultipleInheriJava d = new MultipleInheriJava();
		d.show();
	}
}