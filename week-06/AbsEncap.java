//program to demonstrate the Access Specifiers in java//
class AccessSpecifierDemo{
    private
    int PriVar;
    protected
    int ProVar;
    public
    int PubVar;
    
    void setVar(int priVal,int pubVal,int proVal){
        PriVar=priVal;
        ProVar=proVal;
        PubVar=pubVal;


    }
    void getVar(){
        System.out.println("The Private Variable  is:"+PriVar);
        System.out.println("The Public Variable is:"+PubVar);
        System.out.println("The Protected Variable is:"+ProVar);
        
    }
}
public class AbsEncap{

    public static void main(String[] args){
        AccessSpecifierDemo obj = new AccessSpecifierDemo();
        obj.setVar(12, 15, 20);
        obj.getVar();

    }
}