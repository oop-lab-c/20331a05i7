//program to perform arithmetic operations using switch case//

#include<iostream>
using namespace std;
int main() {
    int a,b;
    char op;
    cout<<"enter the two operands"<<endl;
    cin>>a>>b;
    cout<<"enter the operator"<<endl;
    cin>>op;
    switch(op){
        case '+' : cout<<"the addition is"<<a+b<<endl;
                   break;
        case '-' : cout<<"the sutraction is"<<a-b<<endl;
                   break;
        case '*' : cout<<"the product is"<<a*b<<endl;
                   break;
        case '/' : cout<<"the division is"<<a/b<<endl;
                   break;
        case '%' : cout<<"the modulus is"<<a%b<<endl; 
                   break;
        default  : cout<<"the operator entered is invalid"<<endl;
                   break;
    }
    return 0;
}