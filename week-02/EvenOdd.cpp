//program to display whether the number is even or odd//

#include <iostream>
using namespace std;
void CheckNumber(int x) {
    if (x % 2 == 0) {
        cout << "The input number is even"<<endl;
    } else {
        cout << "The input number is odd"<<endl;
    }
}
int main() {
    int x;
    
    cout << "Enter an integer number to check "<<endl;
    cin >> x;

    CheckNumber(x);
    return 0;
}