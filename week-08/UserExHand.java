// Program to demonstrate the userdefined exceptions in java//
import java.util.*;
public class UserExHand {
    public static void main(String[] args)
    {
        try        //try block
        {
            throw new MyException(5); 
        }
        catch(Exception e)  
        {
            System.out.println(e);
        }
        finally    //finally block
        {
            System.out.println("User defined exceptions are handled...");
        }
    }
}
class MyException extends Exception
{
    int a;
    MyException(int b)
    {
     a=b;
     System.out.println(a+b);
    }
}