//pogram to know the usage of friend function and scope resolution and inline function//
#include<iostream>
using namespace std;
class Box{
    private:
    float length,width,height;
    public:
    void setBoxDimensions(float l,float w,float h){
        length=l;
        width=w;
        height=h;
    }
    void boxArea(float l,float w)
    {
      cout<<"the area is "<<l*w<<endl;
    }
    void boxVolume(float,float,float);
    friend void displayBoxDimensions(Box obj);
};
void Box :: boxVolume(float l,float w,float h)
{
    cout<<"the volume of the box "<<l*w*h<<endl;
}
void displayBoxDimensions(Box obj){
    cout<<"length of the box is "<<obj.length<<endl;
    cout<<"width of the box is "<<obj.width<<endl;
    cout<<"height of the box is "<<obj.height<<endl;
}
inline void displayWelcomeMsg(){
        cout<<"message"<<endl;
}

int main(){
    Box obj;
    float l,w,h;
    cout<<"enter the length and width of the box"<<endl;
    cin>>l>>w;
    obj.boxArea(l,w);
    cout<<"enter the height"<<endl;
    cin>>h;
    obj.boxVolume(l,w,h);
    obj.setBoxDimensions(l,w,h);
    displayBoxDimensions(obj);
    displayWelcomeMsg();
    return 0;
}