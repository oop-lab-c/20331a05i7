//program to write a C++ class "student" and know the constructor and destructor usage//
#include<iostream>
using namespace std; 
class Student{
    public:
    string name;
    string clgName;
    int rollNo;
    int clgCode;
    double semPercentage;
Student(){}

    void displayName( string name)
    {
        cout<<"the name of student is "<<name<<endl;
    }
    void display( string clgName)
    {
        cout<<"the name of Clg is "<<clgName<<endl;
    }
    void displayNo(int rollNo,int clgCode)
    {
        cout<<"the roll no of the student is "<<rollNo<<endl;
        cout<<"the clgcode of the student is "<<clgCode<<endl;
    }
    void displaypercent(double semPercentage)
    {
        cout<<"the sem result of the student is "<<semPercentage<<endl;
    }
    ~Student(){
        cout<<"Destructor"<<endl;
    }
};


int main()
{
    Student obj;
    int rollNo;
    int clgCode;
    double semPercentage;
    string name;
    string clgName;
    cout<<"enter the name of student "<<endl;
    cin>>name;
    obj.displayName(name);
    cout<<"enter name of clg"<<endl;
    cin>>clgName;
    obj.display(clgName);
    cout<<"enter the roll no and clgcode"<<endl;
    cin>>rollNo>>clgCode;
    obj.displayNo(rollNo,clgCode);
    cin>>semPercentage;
    obj.displaypercent(semPercentage);
    return 0;
}
