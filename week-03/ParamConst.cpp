//program to demonstrate default and parameterised constructor//
#include<iostream>
using namespace std;
class Student{
    public:
     string clgName,name;
     int clgCode;
     double percent;
     Student()
     {
         clgName="MVGR";
         clgCode=33;
         cout<<"name of CLG "<<clgName<<endl;
         cout<<"clg code "<<clgCode<<endl;
         
     }
     Student(string Name,double sempt)
     {
         name=Name;
         percent=sempt;
         cout<<"name of student"<<name<<endl;
         cout<<"the sem percent"<<percent<<endl;
     }
     ~Student(){
         cout<<"destructor"<<endl;
        
     }
};
int main(){
    string name;
    double percent;
    cout<<"enter name of student"<<endl;
    Student obj;
    Student obj1(name,percent=89.98);
    return 0;
}